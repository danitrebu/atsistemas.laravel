<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

/* Contact form */
Route::get('/contact', 'ContactController@index')->name('contact');
/* Send contact message */
Route::post('/send', 'ContactController@send')->name('send');

/* Show all posts */
Route::get('/blog', 'BlogController@index')->name('blog');

/* Show post */
Route::get('/post/{id}', 'PostController@index')->name('post');

/* Create comment */
Route::post('/comment', 'CommentController@create')->name('comment');
/* Delete comment */
Route::get('/comment/delete/{id}', 'CommentController@destroy')->name('deleteComment');
/* Edit comment (redirect to view in controller) */
Route::get('/comment/edit/{id}', 'CommentController@edit')->name('editComment');
/* Update comment (actually edit comment in controller) */
Route::post('/comment/update/{id}', 'CommentController@update')->name('updateComment');