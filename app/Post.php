<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [];

    public function index()
    {
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

}
