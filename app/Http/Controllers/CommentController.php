<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use DB;


class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        DB::table('comments')->insert([
            'post_id' => intval($request->input('post_id')),
            'email' => $request->input('email'),
            'comment' => $request->input('message'),
            'comment_id' => null,
            'isResponse' => false,
            'created_at' => date('Y-m-d H:i:m')
        ]);

        return back()->with('success', 'Se ha insertado el mensaje');
        /*return redirect()->route('post', [
            'post' => $request->input('post_id')
        ]);*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::find($id);
        return view('edit-comment', ['id' => $id, 'comment' => $comment->comment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);
        $comment->comment = $request->input('message');
        $comment->updated_at = date('Y-m-d H:i:m');
        $comment->save();

        return redirect()->route('post', ['id' => $comment->post_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //DB::table('comments')->where('id', '=', $id)->delete(
        $comment = Comment::find($id);
        $comment->delete();

        // return to previous view (post) and sends flash message
        return back()->with('success', 'Se ha borrado el mensaje');
    }
}
