<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Faker;

class BlogController extends Controller
{
    public function index()
    {
        $posts = DB::table('posts')->get();
        return view('blog', ['posts' => $posts]);
    }

    public function show()
    {

    }
}
