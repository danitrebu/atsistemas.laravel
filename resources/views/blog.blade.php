@extends('layouts.app')

@section('content')

        <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->

</head>
<body>
<section id="contact">

    <h1 class="section-header">Entradas del blog</h1>

    <div class="contact-wrapper">
        @foreach ($posts as $post)
            <h4>{{ $post->title }}</h4>
            {{ mb_strimwidth($post->article, 0, 200, "...") }}<br><br>
            <a href="{{ route('post', ['post' => $post->id]) }}" style="background-color:lightcoral;color:white;padding:5px;">Leer más...</a>
            <hr>
        @endforeach
    </div>

</section>


</body>
</html>


@endsection