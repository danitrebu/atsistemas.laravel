@extends('layouts.app')

@section('content')
        <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->

</head>
<body>
<section id="contact">

    <!-- if there is a flash message show it -->
    @include('flash-message')

    <h1 class="section-header">Contactar con atSistemas</h1>

    <div class="contact-wrapper">


        {{ Form::open(array('url' => route('send'),'method' => 'post')) }}
        @csrf
        <table>
            <tr>
                <td>{{ Form::label('name', 'Nombre y apellidos', ['class' => 'label']) }}</td>
                <td>{{ Form::text('name', 'John Doe', ['name' => 'name']) }}</td>
                @error('name')
                <span style="color:red">El nombre no puede estar vacío</span>
                @enderror
            </tr>
            <tr>
                <td>{{ Form::label('email', 'Correo electrónico', ['class' => 'label']) }}</td>
                <td>{{ Form::text('email', 'test@email.com', ['name' => 'email']) }}</td>
                @error('email')
                <span style="color:red" n>El email es obligatorio</span>
                @enderror
            </tr>
            <tr>
                <td>Mensaje</td>
                <td>{{ Form::textarea('message', 'Mensaje...' ) }}</td>
                @error('message')
                <span style="color:red">El mensaje no puede estar vacío</span>
                @enderror
            </tr>
            <tr>
                <td></td>
                <td> {{ Form::submit('Contactar con atsistemas') }}</td>
            </tr>
        </table>

        {{ Form::close() }}

    </div>

</section>


</body>
</html>
@endsection