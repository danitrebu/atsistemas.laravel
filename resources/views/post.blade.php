@extends('layouts.app')

@section('content')

        <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
</head>
<body>
<section id="contact">

    <!-- if there is a flash message show it -->
    @include('flash-message')

    <h1 class="section-header">Entrada</h1>
    <div class="contact-wrapper">
        <h4>{{ $post->title }}</h4>
        <p>{{ $post->article }}</p>
        <br>
        <p>Publicado el {{ date('d-m-Y', strtotime($post->created_at)) }}</p>
    </div>

    <hr>

    <form method="post" role="form" action=" {{ route('comment') }} "
          style="border:1px solid black;padding: 15px;background-color: lightgrey;">
        @csrf
        <input type="hidden" name="post_id" value="{{ $post->id }}">
        <h4>Deja tu opinión</h4>
        <table style="width:100%;">
            <tr>
                <td>Correo electrónico</td>
                <td><input type="email" name="email" placeholder="john@doe.com" style="width:100%;"></td>
            </tr>
            <tr>
                <td>Mensaje</td>
                <td><textarea name="message" placeholder="Mensaje..." style="width:100%;"></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Enviar"></td>
            </tr>
        </table>
        <br>
    </form>

    <br>

    @foreach ($comments as $comment)

        <section class="comments">
            <article class="comment">
                <div class="comment-body" style="border:1px solid black;padding:15px;background-color: lightgrey">
                    <div class="avatar">
                        <img src="https://wdcolledge.com/wp-content/uploads/2018/04/placeholder.png"
                             style="width:50px;">
                        <p>{{ $comment->email }}</p>
                    </div>
                    <div class="text">
                        <p>{{ $comment->comment }}</p>
                    </div>
                    <span class="attribution">por <a href="#non">{{ $comment->email }}</a>
                        el {{ date('d-m-Y', strtotime($comment->created_at)) }}</span>
                    <br>
                    @if($comment->updated_at)
                        <span class="attribution">actualizado el {{ $comment->updated_at }}</span>
                    @endif
                    <br><br>
                    <a href="{{ route('editComment', ['id' => $comment->id]) }}" class="alert-info">Editar
                        comentario</a>
                    <a href="{{ route('deleteComment', ['id' => $comment->id]) }}" class="alert-danger">Borrar
                        mensaje</a>
                </div>
            </article>
        </section>​
    @endforeach

</section>
</body>
</html>
@endsection