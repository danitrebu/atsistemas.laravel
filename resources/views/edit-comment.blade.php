@extends('layouts.app')

@section('content')

        <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
</head>
<body>
<section id="contact">

    <h1 class="section-header">Editar comentario</h1>

    <div class="contact-wrapper" style="background-color:lightgrey;padding:15px;border:1px solid black;">
        <p>Reescriba el comentario y pulse el botón para editarlo</p>

        <form method="post" action="{{ route('updateComment', ['id' => $id]) }}">
            @csrf
            <textarea style="width:100%;" name="message" placeholder="{{ $comment }}"></textarea>
            <br>
            <input type="submit" value="Editar comentario">
        </form>

    </div>

    <hr>

    <br>


</section>
</body>
</html>
@endsection